
package programmes;

import gestionprojets.Client;
import gestionprojets.Projet;

import static utilitaires.UtilDate.aujourdhuiChaine;
import static gestionprojets.Entreprise.getTousLesClients;

public class Programme03 {

    public  void executer() {
        
        System.out.println();
        
        System.out.printf(" Liste des Projets par Clients à la date du %8s: \n\n", aujourdhuiChaine());
        
        for ( Client unClient  : getTousLesClients()){
        
              System.out.print("  ");unClient.afficher(); System.out.println();
              
              System.out.println();
              
              int nbProjCli=0;
              
              for(Projet unProjet : unClient.getLesProjets()){
              
                  nbProjCli++; 
                  System.out.print("       - ");unProjet.afficher();   System.out.println();
              }
              
              if ( nbProjCli==0){ System.out.println("       -- Pas de Projet -- "); }else{ nbProjCli=0;}
              
              System.out.println();
        
        }
          
        System.out.println();
    }
 
}

