package programmes;

import gestionprojets.Salarie;
import static gestionprojets.Entreprise.getTousLesPoles;
import gestionprojets.Pole;

public class Exemple04 {

    public  void executer() {
      
     
       System.out.printf("\nListe de salariés à la date du: %-8s \n\n", utilitaires.UtilDate.aujourdhuiChaine());
       
       for(Pole pole : getTousLesPoles()){
       
           System.out.println(pole.getNomPole()); System.out.println("");
            
           for(Salarie sal: pole.getLesSalaries()){
        
              sal.afficher();
              System.out.println();
        
            }
        
            System.out.println();
        
        }
        System.out.println();
        
       
    }
}

