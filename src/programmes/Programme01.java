package programmes;

import        gestionprojets.Salarie;
import static gestionprojets.Tris.trierParSalairesDecroissants;
import static gestionprojets.Entreprise.getTousLesSalaries;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilDate.aujourdhuiChaine;

public class Programme01 {

    public  void executer() {
        
       
      System.out.printf("\n Les trois salariés de plus de 24 ans ayant les meilleurs salaires à la date du: %-8s \n\n", aujourdhuiChaine());
      
      int nbSalAff=0;
       
      for(Salarie sal: trierParSalairesDecroissants(getTousLesSalaries())){
        
        if (ageEnAnnees(sal.getDateNaiss())>24 ) { 
            
            System.out.print(" ");sal.afficher(); System.out.printf("  %-8s",sal.getLePole().getCodePole());
            System.out.println(); nbSalAff++;
        }
        if (nbSalAff==3) break;
      }
        
      System.out.println();
    }
    
    
    
}

