
package gestionprojets;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static utilitaires.UtilDate.convDateVersChaine;

/**
 *
 * @author rsmon
 */

public class Projet {
        
    //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    private String codeProj;
    private String descProj;
    private Date   dateDebP;
    private Date   dateFinP;
    private Float  montantDevis;
    
    private Client leClient;
    
    private List<Affectation> lesAffectations=new LinkedList();
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public String getCodeProj() {
        return codeProj;
    }
    
    public void setCodeProj(String codeProj) {
        this.codeProj = codeProj;
    }
    
    public String getDescProj() {
        return descProj;
    }
    
    public void setDescProj(String descProj) {
        this.descProj = descProj;
    }
    
    public Date getDateDebP() {
        return dateDebP;
    }
    
    public void setDateDebP(Date dateDebP) {
        this.dateDebP = dateDebP;
    }
    
    public Date getDateFinP() {
        return dateFinP;
    }
    
    public void setDateFinP(Date dateFinP) {
        this.dateFinP = dateFinP;
    }
    
    public Client getLeClient() {
        return leClient;
    }
    
    public void setLeClient(Client leClient) {
        this.leClient = leClient;
    }

    public Float getMontantDevis() {
        return montantDevis;
    }

    public void setMontantDevis(Float montantDevis) {
        this.montantDevis = montantDevis;
    }
    
    
    
    public List<Affectation> getLesAffectations() {
        return lesAffectations;
    }

    
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Constructeurs">
    
    public Projet() {
    }
    
    public Projet(String codeProj, String descProj, Date dateDebP, Date datefinP, Float montantDevis) {
        
        this.codeProj     = codeProj;
        this.descProj     = descProj;
        this.dateDebP     = dateDebP;
        this.dateFinP     = datefinP;
        this.montantDevis = montantDevis;
    }
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Méthodes Métiers">
    
    public void afficher(){
    
        System.out.printf("%-10s %-50s %-8s %-8s %10.2f €",
                          codeProj,
                          descProj,
                          convDateVersChaine(dateDebP),
                          convDateVersChaine(dateFinP),
                          montantDevis
        );
    
    }
    
    //</editor-fold>
}




