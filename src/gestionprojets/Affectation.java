
package gestionprojets;

import java.io.Serializable;
import java.util.Date;

public class Affectation implements Serializable {

    private Salarie leSalarie;
    private Projet  leProjet;
 
    private Date    dateaff;
    private Boolean pilote;
     
    //<editor-fold defaultstate="collapsed" desc="Constructeurs">
    
    public Affectation() {}
    
    public Affectation( Projet leProjet,Salarie leSalarie) {
        
        this.leProjet = leProjet;
        this.leProjet.getLesAffectations().add(this);
        
        this.leSalarie = leSalarie;
        this.leSalarie.getLesAffectations().add(this);
        
    }
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public Boolean getPilote() {
        return pilote;
    }

    public void    setPilote(Boolean pilote) {
        this.pilote = pilote;
    }

    public Date getDateaff() {
        return dateaff;
    }
    public void setDateaff(Date dateaff) {
        this.dateaff = dateaff;
    }

    public Projet getLeProjet() {
        return leProjet;
    }
    public void setLeProjet(Projet leProjet) {
        this.leProjet = leProjet;
    }

    public Salarie getLeSalarie() {
        return leSalarie;
    }
    public void setLeSalarie(Salarie leSalarie) {
        this.leSalarie = leSalarie;
    }
    
    
    //</editor-fold>
        
}
