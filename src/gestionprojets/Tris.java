package gestionprojets;

import static java.util.Collections.sort;
import java.util.Comparator;
import java.util.List;

public class Tris {
    
    public static List<Salarie>  trierParNomPrenom(List<Salarie> lj){
          sort(lj, new ComparateurParNomPrenom());
          return lj;
    }
    
    public static  List<Salarie>  trierParSexeNomPrenom(List<Salarie> lj){
          sort(lj, new ComparateurParSexeNomPrenom());
          return lj;
    }
     
    public static  List<Salarie> trierParNom(List<Salarie> lj){
          sort(lj, new ComparateurParNom());
          return lj;
    }
    
    public static List<Salarie> trierParDateDeNaissance(List<Salarie >  lj){
    
           sort(lj, new ComparateurParDateDeNaissance() );
           return lj;
    }
    
   
    public static List<Salarie> trierParSalairesDecroissants(List<Salarie> ls){
    
          sort(ls, new  ComparateurParSalairesDecroissants() );
          return ls;     
    } 
    
    
     public static List<Affectation>  trierParDateAffectation(List<Affectation> lj){
          sort(lj, new ComparateurAffectationParDate());
          return lj;
    }
    
    public static class  ComparateurParNomPrenom implements Comparator{

      @Override
      public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Salarie j  = (Salarie)  t ;
        Salarie j1 = (Salarie)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom/prénom">
            
            resultat=  j.getNom().compareTo(j1.getNom());
            if (resultat==0) resultat= j.getPrenom().compareTo(j1.getPrenom());
            
        //</editor-fold>
         
        return resultat;  
      }    
    }
    
    public static  class ComparateurParNom implements Comparator{

      @Override
      public int compare(Object t, Object t1) {
        
        int resultat=0; 
        
         Salarie j  = (Salarie)  t ;
         Salarie j1 = (Salarie)  t1;
            
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom">
            
            resultat=  j.getNom().compareTo(j1.getNom());
            
            //</editor-fold>
        
       
        
        return resultat;  
      }    
    }
   
    public static  class ComparateurParSexeNomPrenom implements Comparator{

     @Override
     public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Salarie j  = (Salarie)  t ;
        Salarie j1 = (Salarie)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par sexe/nom/prénom">
           
        resultat= j.getSexe().compareTo(j1.getSexe());
        if ( resultat==0){
             resultat=  j.getNom().compareTo(j1.getNom());
             if (resultat==0) resultat= j.getPrenom().compareTo(j1.getPrenom());
         }            
        
        //</editor-fold>
      
        return resultat;  
     }    
    }
   
    public static class  ComparateurParSalairesDecroissants implements Comparator{

      @Override
      public int compare(Object t, Object t1) {
        
         int resultat=0;
      
          Salarie j  =  (Salarie)   t ;
          Salarie j1 =  (Salarie)   t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par poids">
        
           if        ( j.getSalaire()   < j1.getSalaire() )   resultat =   1;
           else if   ( j.getSalaire()   > j1.getSalaire() )   resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
       }    
}
 
   
    
    public static class  ComparateurParDateDeNaissance implements Comparator{

       @Override
       public int compare(Object t, Object t1) {
        
          int resultat=0;
      
           Salarie j  = (Salarie)   t ;
           Salarie j1 = (Salarie)   t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par date de naissance">
        
            if       ( j.getDateNaiss().before (j1.getDateNaiss()) )   resultat =   1;
            else if  ( j.getDateNaiss().after  (j1.getDateNaiss()) )   resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
       } 
    
    }
    
    
     public static class  ComparateurAffectationParDate implements Comparator{

      @Override
      public int compare(Object t, Object t1) {
        
         int resultat=0;
      
          Affectation j  =  (Affectation)   t ;
          Affectation j1 =  (Affectation)   t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR ">
        
          if        ( j.getDateaff().after(j1.getDateaff())  )   resultat =   1;
            
            //</editor-fold>
        
            return resultat;  
       }   
     }
    
}    




